import 'package:flutter/material.dart';
import 'package:food_finder_app/views/widgets/favCard.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Favorites extends StatefulWidget {
  const Favorites({super.key});

  @override
  State<Favorites> createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  void initState(){
    super.initState();
    getRecipes();
  }
  // Future<void> getRecipes() async{
  //   _recipes= await RecipeApi.getRecipe();
  //   setState(() {
  //     _isLoading=false;
  //   });
  //   print(_recipes);
  // }
  // List<dynamic> recipedata = [];
  String jsonString = '';
  List recipes=[];
  List recipeNames=[];
  List recipesTime=[];
  List recipeImage=[];
  List recipeRating=[];
  List recipesUrl=[];
  Future<void> getRecipes() async {
    String uri = 'https://dummyjson.com/recipes';
    try {
      var response = await http.get(Uri.parse(uri));
      // print(response.body);
      setState(() {
        _isLoading=false;
        Map temp = jsonDecode(response.body);
        // recipedata = jsonDecode(response.body);
        // print(recipedata);
        // print(temp);
        recipes = temp['recipes'];
        // print(recipes);
        recipeNames = recipes.map((recipe) => recipe['name']).toList();
        // print(recipeNames[0]);
        recipesTime = recipes.map((recipe) => recipe['cookTimeMinutes']).toList();
        // print(recipesTime[0]);
        recipeRating = recipes.map((recipe) => recipe['rating']).toList();
        // print(recipeRating[0]);
        recipesUrl = recipes.map((recipe) => recipe['image']).toList();
        // print(recipesUrl[0]);
      });
    } catch (e) {
      print(e);
    }
  }
  bool _isLoading=true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xffEFBC9B),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.restaurant_menu_outlined),
            SizedBox(
              width: 10,
            ),
            Text("Favorites Food Recipe"),
            SizedBox(
              width: 60,
            ),
          ],
        ),
      ),
        body: Container(
          child: ListView.builder(
            // itemCount:recipes.length,
            // itemCount: 3,
            itemCount: recipes.length > 5 ? 5 : recipes.length,
            itemBuilder: (context, index) {
              return FavCard(
                  title: recipeNames[index],
                  cookTime: recipesTime[index].toString(),
                  rating:recipeRating[index].toString(),
                  thumbnailUrl:recipesUrl[index]);
            },
          )
        )
    );

  }
}
// _isLoading
// ? Center(child: CircularProgressIndicator())
//     : ListView.builder(
// itemCount:1,
// // itemCount: 1,
// itemBuilder: (context, index) {
// return FavCard(
// title: "recipeNames[index]",
// cookTime: "recipesTime[index].toString()",
// rating:"recipeRating[index].toString()",
// thumbnailUrl:"recipesUrl[index]");
// },
// )