import 'package:flutter/material.dart';
import 'package:food_finder_app/views/favorites.dart';
import 'package:food_finder_app/views/foodCategory.dart';
import 'package:food_finder_app/views/home.dart';
import 'package:food_finder_app/views/settings.dart';

import 'my_drawer_header.dart';

class appBar extends StatefulWidget {
  const appBar({super.key});

  @override
  State<appBar> createState() => _appBarState();
}

class _appBarState extends State<appBar> {
  var currentPage=DrawerSections.home;
  @override
  Widget build(BuildContext context) {
    var container;
    if (currentPage== DrawerSections.home){
      container=HomePage();
    }else  if (currentPage== DrawerSections.foodCategory){
      container=FoodCategory();
    }else  if (currentPage== DrawerSections.settings){
      container=Settings();
    }
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xffEFBC9B),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.restaurant_menu_outlined),
          SizedBox(
            width: 10,
          ),
          Text("Food Recipe Finder"),
          SizedBox(
            width: 60,
          ),
          InkWell(
             child: Image.asset("assets/images/heart.png",height: 25,width: 25,),
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Favorites(),
                  ));
            },
          )
        ],
      ),
    ),
      body:container,
     drawer: Drawer(
       child: SingleChildScrollView(
         child: Container(
           child: Column(
             children: [
               MyHeaderDrawer(),
               MyDrawerList(),
             ],
           ),
         ),
       ),
     ),
    );
  }
  Widget MyDrawerList(){
    return Container(
      padding: EdgeInsets.only(top: 15),
      child: Column(
        children: [
          menuItem(1,"Home",Icons.home,
          currentPage==DrawerSections.home?true:false),
          Divider(),
          menuItem(2,"Food Category",Icons.food_bank_outlined,
              currentPage==DrawerSections.foodCategory?true:false),
          Divider(),
          menuItem(3,"Settings",Icons.settings,
              currentPage==DrawerSections.settings?true:false),
        ],
      ),
    );
  }
  Widget menuItem(int id,String title,IconData icon,bool selected){
    return Material(
      color: selected ? Colors.grey[300]:Colors.transparent,
        child: InkWell(
        onTap: (){
          Navigator.pop(context);
          setState(() {
            if(id==1){
              currentPage=DrawerSections.home;
            }else if(id==2){
              currentPage=DrawerSections.foodCategory;
            } else if(id==3){
              currentPage=DrawerSections.settings;
            }
          });
        },
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Row(
            children: [
              Expanded(
                  child: Icon(icon,size: 30,color: Colors.black,)),
              Expanded(
                  flex:3,
                  child: Text(title,style: TextStyle(color: Colors.black,fontSize: 16),))
            ],
          ),
        ),
      ));
  }
}
enum DrawerSections{
  home,
  foodCategory,
  settings
}
