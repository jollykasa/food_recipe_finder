import 'package:flutter/material.dart';

class FoodCategory extends StatelessWidget {
  const FoodCategory({super.key});

  @override
  Widget build(BuildContext context) {
    var arrNames=["Vegetarian","Vegan","Gluten-Free","Dairy-Free","Non-Vegetarian"];
    var imageName=["plant-based.png","vegan.png","gluten-free.png","dairy-free.png","turkey.png"];
    return Container(
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.only(top:8.0,bottom: 8.0),
              child:Text("Food Category",style: TextStyle(fontSize: 40,fontWeight: FontWeight.bold),)
          ),
          Expanded(
            child: ListView.builder(
                itemCount: arrNames.length,
                itemBuilder: ((context, index) {
                  return Container(
                      child:Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Card(
                            elevation: 7,
                            child: InkWell(
                              onTap:(){

                              },
                              child: Container(
                                margin: const EdgeInsets.all(10),
                                padding: const EdgeInsets.all(10),
                                height: 50,
                                // color: Colors.deepPurple,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Image.asset('assets/images/'+imageName[index],width: 30,height: 30,),
                                          SizedBox(width: 150,),
                                          Text(arrNames[index], style:TextStyle(fontSize: 20,fontWeight: FontWeight.w500)),
                                          ]),
                                  ],
                                ),
                              ),
                            ),
                          )));
                })),
          ),
        ],
      ),
    );
  }
}
