import 'package:flutter/material.dart';

class MyHeaderDrawer extends StatefulWidget {
  const MyHeaderDrawer({super.key});

  @override
  State<MyHeaderDrawer> createState() => _MyHeaderDrawerState();
}

class _MyHeaderDrawerState extends State<MyHeaderDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xffFFB38E),
      width: double.infinity,
      height: 200,
      padding: EdgeInsets.only(top:20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            height: 70,
              child:Icon(Icons.restaurant_menu,size: 60,)
          ),
          Divider(
            indent: 15,
            endIndent: 15,
            thickness: 2,
          ),
          Text("Food Recipe Finder",style:TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
          Divider(
            indent: 15,
            endIndent: 15,
            thickness: 2,
          ),
        ],
      ),
    );
  }
}
