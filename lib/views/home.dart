import 'dart:math';

import 'package:flutter/material.dart';
import 'package:food_finder_app/models/recipe.api.dart';
import 'dart:convert';
import 'package:food_finder_app/models/recipe.dart';
import 'package:http/http.dart' as http;
import 'package:food_finder_app/views/widgets/recipe_card.dart';
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController searchController=new TextEditingController();
  // late List <Recipe> _recipes;
  bool _isLoading=true;
  void initState(){
    super.initState();
    getRecipes();
  }
  // Future<void> getRecipes() async{
  //   _recipes= await RecipeApi.getRecipe();
  //   setState(() {
  //     _isLoading=false;
  //   });
  //   print(_recipes);
  // }
  // List<dynamic> recipedata = [];
  String jsonString = '';
  List recipes=[];
  List recipeNames=[];
  List recipesTime=[];
  List recipeImage=[];
  List recipeRating=[];
  List recipesUrl=[];
  Future<void> getRecipes() async {
    String uri = 'https://dummyjson.com/recipes';
    try {
      var response = await http.get(Uri.parse(uri));
      // print(response.body);
      setState(() {
        _isLoading=false;
        Map temp = jsonDecode(response.body);
        // recipedata = jsonDecode(response.body);
        // print(recipedata);
        // print(temp);
        recipes = temp['recipes'];
        // print(recipes);
        recipeNames = recipes.map((recipe) => recipe['name']).toList();
        // print(recipeNames[0]);
        recipesTime = recipes.map((recipe) => recipe['cookTimeMinutes']).toList();
        // print(recipesTime[0]);
        recipeRating = recipes.map((recipe) => recipe['rating']).toList();
        // print(recipeRating[0]);
        recipesUrl = recipes.map((recipe) => recipe['image']).toList();
        // print(recipesUrl[0]);
      });
    } catch (e) {
      print(e);
    }
  }
  @override
  Widget build(BuildContext context) {
    // var foodname=["chicken","momo","pizza"];
    // final _random=new Random();
    // var food=foodname[_random.nextInt(foodname.length)];
    return Scaffold(
      body:Column(
        children: [
          Container(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  margin: EdgeInsets.symmetric(horizontal: 10,vertical: 9),
                  decoration: BoxDecoration(
                    color:Colors.black12,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: (){
                          if((searchController.text).replaceAll("", "")== ""){
                            print("Blank search");
                          }else{
                            print(searchController.text);
                          }
                          // Navigator.pushReplacementNamed(context, "/loading",arguments: {
                          //   "searchText":searchController.text,
                          // });
                          // print("Filter");
                          },
                        child: Container(child: Icon(Icons.search),
                        margin: EdgeInsets.fromLTRB(3, 0, 15, 0)),
                      ),
                      Expanded(
                        child: TextField(
                          controller: searchController,
                          onSubmitted:(value){
                            print(value);
                          } ,
                          textInputAction: TextInputAction.search,
                          decoration: InputDecoration(
                            hintText: "Search Food Recipe",
                            border: InputBorder.none,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Column(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: [
          // Text(
          //     "What Do You Want To Cook Today?",style: TextStyle(fontSize: 25,fontWeight: FontWeight.w500)
          // ),
          // Text(
          //     "Lets Cook Something!",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500)
          // ),
          //   ],
          // ),
          Expanded(
              flex: 6,
              child: _isLoading
              ? Center(child: CircularProgressIndicator())
              : ListView.builder(
            itemCount: recipes.length,
            // itemCount: 1,
            itemBuilder: (context, index) {
              return RecipeCard(
                  title: recipeNames[index],
                  // cookTime: recipedata[index]['cookTimeMinutes'],
                  cookTime: recipesTime[index].toString(),
                  rating:recipeRating[index].toString(),
                  thumbnailUrl:recipesUrl[index]);
              // thumbnailUrl: recipedata[index]['image']);
            },
          ))
        ],
      )
    );
  }
}
