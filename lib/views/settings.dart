import 'package:flutter/material.dart';

class Settings extends StatelessWidget {
  const Settings({super.key});

  @override
  Widget build(BuildContext context) {
    var arrNames=["Password Setting","Edit Food time","Cook Time Alarm","Alarm Ringtone","Additional Settings"];
    return Container(
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.only(top:8.0,bottom: 8.0),
              child:Text("Settings",style: TextStyle(fontSize: 40,fontWeight: FontWeight.bold),)
          ),
          Expanded(
            child: ListView.builder(
                itemCount: arrNames.length,
                itemBuilder: ((context, index) {
                  return Container(
                      child:Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Card(
                            elevation: 7,
                            child: InkWell(
                              onTap:(){

                              },
                              child: Container(
                                margin: const EdgeInsets.all(10),
                                padding: const EdgeInsets.all(10),
                                height: 50,
                                // color: Colors.deepPurple,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(arrNames[index], style:TextStyle(fontSize: 20,fontWeight: FontWeight.w500)),
                                          Icon(Icons.arrow_forward_ios)]),
                                  ],
                                ),
                              ),
                            ),
                          )));
                })),
          ),
        ],
      ),
    );
  }
}
